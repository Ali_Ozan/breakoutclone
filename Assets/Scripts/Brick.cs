using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public Material[] _materials;
    Renderer _renderer;
    public int hits = 1;
    public int points = 100;
    public Vector3 rotator;

    void Start()
    {
        transform.Rotate(rotator * (transform.position.x + transform.position.y) * 0.07f);
        _renderer = GetComponent<Renderer>();
        _renderer.enabled = true; // not necessary but in case its not...
       
         _renderer.sharedMaterial = _materials[hits - 1];
    }

    void Update()
    {
        transform.Rotate(rotator * Time.deltaTime);//deltatime'� kulland���m i�in kullan�lan yerde framerate ne olursa olsun ayn� h�zla d�ner
    }

    void OnCollisionEnter(Collision collision)
    {
        hits--;
        //Score points
        if (hits <=0)
        {
            GameManager.Instance.Score += points;
            Destroy(gameObject);
            return;
        }
        _renderer.sharedMaterial = _materials[4]; //vurulan brick anl�k beyaz olacak
        Invoke("RestoreMaterial", 0.05f); //o "anl�k" beyazlaman�n s�resi buradan ayarlanacak ve yine mevcut hitpointine g�re materialine d�necek
    }

    void RestoreMaterial()
    {
        _renderer.sharedMaterial = _materials[hits - 1];
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    float _speed = 30f;
    Rigidbody _rigidBody;
    Renderer _renderer;
    Vector3 _velocity;
    //Vector3 _initialPosition;
    float noHitRemainingTime = 10;//this time will be tracked in case of the ball going horizontal and not hitting anything. If the time is up, a small kick will change its direction
    Color originalColor;

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        //_initialPosition = _rigidBody.position;
        Invoke("Launch", 1f);
        _renderer = GetComponent<Renderer>();
        originalColor = _renderer.material.color;
    }

    void Launch ()
    {
        _rigidBody.velocity = new Vector3(1, -3, 0) * _speed;
    }
    void FixedUpdate()
    {
        _rigidBody.velocity = _rigidBody.velocity.normalized * _speed;
        _velocity = _rigidBody.velocity;
        if (! _renderer.isVisible) //ekrandan ��kt�ysa
        {
            GameManager.Instance.Balls--;
            Destroy(gameObject);
        }
        
        /*if (_rigidBody.position.y < -40f)
        {
            _rigidBody.velocity = new Vector3(0, 0, 0);
            _rigidBody.position = _initialPosition;
            Invoke("ResetVelocity", 1f);
        }*/
        noHitRemainingTime -= Time.deltaTime;
        if (noHitRemainingTime < 3)
            TurnBallRed();
        if (noHitRemainingTime <= 0)
        {
            KickTheBall();
            _renderer.material.color = originalColor;
        }
    }

    void TurnBallRed()
    {
        _renderer.material.color = Color.red;
    }

    void KickTheBall()
    {
        _rigidBody.velocity = new Vector3(1, 3, 0) * _speed;
        noHitRemainingTime = 10;
    }
    void ResetVelocity()
    {
        _rigidBody.velocity = new Vector3(1, -3, 0) * _speed;
    }

    void OnCollisionEnter(Collision collision)
    {
        _rigidBody.velocity = Vector3.Reflect(_velocity, collision.contacts[0].normal);
        if (collision.gameObject.layer == 6 || collision.gameObject.layer == 7) //The time is reset if the collided objedct is a brick or the paddle
        {
            noHitRemainingTime = 10;
            _renderer.material.color = originalColor;
        }    
    }
}

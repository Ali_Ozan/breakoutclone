using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody _rigidBody;
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Vector3 thePositionMouseWillTakeThePlayer = new Vector3(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 50)).x, -17, 0);
        if (thePositionMouseWillTakeThePlayer.x > 37f) //I had to make this manual obstruction because otherwise the player will be forced through the walls despite the colliders.
            thePositionMouseWillTakeThePlayer.x = 37f;
        else if (thePositionMouseWillTakeThePlayer.x < -37f)
            thePositionMouseWillTakeThePlayer.x = -37f;
        _rigidBody.MovePosition(thePositionMouseWillTakeThePlayer);  //transform.translate ile de hareket sa�lanabilirdi fakat bir rigidbody i�in bunun kullan�lmas� daha do�ru ��nk� physics engine'i de hesaba kat�yor...mu� :) 
    }
}
